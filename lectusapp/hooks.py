# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "lectusapp"
app_title = "Lectusapp"
app_publisher = "Lectus corp"
app_description = "Custom app for website"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "haris.m.aslam@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/lectusapp/css/lectusapp.css"
# app_include_js = "/assets/lectusapp/js/lectusapp.js"

# include js, css files in header of web template
# web_include_css = "/assets/lectusapp/css/lectusapp.css"
# web_include_js = "/assets/lectusapp/js/lectusapp.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

web_include_js = [
	"assets/js/lectustheme/animsition.js",
	"assets/js/lectustheme/slick.js",
	"assets/js/lectustheme/slick-custom.js",
	"assets/js/lectustheme/main.js",
]

web_include_css = [
	# "assets/css/frappe-web.css"
	"assets/css/lectustheme/font-awesome.css",
	"assets/css/lectustheme/animsition.css",
	"assets/css/lectustheme/slick.css",
	"assets/css/lectustheme/util.css",
	"assets/css/lectustheme/main.css"
]

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "lectusapp.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "lectusapp.install.before_install"
# after_install = "lectusapp.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "lectusapp.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"lectusapp.tasks.all"
# 	],
# 	"daily": [
# 		"lectusapp.tasks.daily"
# 	],
# 	"hourly": [
# 		"lectusapp.tasks.hourly"
# 	],
# 	"weekly": [
# 		"lectusapp.tasks.weekly"
# 	]
# 	"monthly": [
# 		"lectusapp.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "lectusapp.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "lectusapp.event.get_events"
# }

